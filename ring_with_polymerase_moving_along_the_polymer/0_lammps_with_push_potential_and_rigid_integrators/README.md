# LAMMPS executable with custom potential (push) and custom fix rigid

The polymerase applies a force using the potential named "**push**" (this is a custom potential developed by Chris Brackley).
In order to incorporate this interaction into the LAMMPS executable:

    1.- Download the most recent version of LAMMPS (currently this has been tested using the **22-Aug-2018** version).
    2.- Unzip the tar file containing the LAMMPS distribution.
    3.- Copy and paste the files "pair_push.cpp" and  "pair_push.h" into the folder "src/" (this folder is part of the standard directories of the lammps distribution).

When the polymerase is fixed in the 3D space this is all you need to start with the simulations. However, if you want to simulate the case when the polymerase moves along the DNA it is necessary to use a **custom fix rigid** developed also by Chris Brackley. In this case, just:

    4.- copy the files "fix_rigid.cpp" and  "fix_rigid.h" into "src/RIGID/". This will replace the ones that comes with the distribution and have the same name.
    5.- Make sure that the **ASPHERE**, **RIGID**, **MOLECULE** and **USER-MISC** packages are installed
    6.- Compile the lammps executable as usual (we renamed the executable as "lmp_serial_22Aug2018_push_potential_and_new_fix_rigid").

To use the fixes, in the LAMMPS script replace the fix rigid line with this:

    fix myfixname mygroupname rigid molecule langevin/rotation 1.0 1.0 1.0 0.001 78458

where the numbers are: *T_start* *T_stop* *T_damp* *T_rot_damp* *seed*.

You can also work out roughly what *T_rot_damp* should be as follows: tau_rot = I/xi_r, where I is moment of inertia, and xi_r is rotational friction. For a solid sphere of diameter d, I=(md^2)/10 and xi_r=pi nu d^3 (where nu is the viscosity).

Copy and paste here both the initial configuration generated before "*initial_configuration_ringdsDNA_with_polymerase_N1000_T100*" and the LAMMPS executable "*lmp_serial_22Aug2018_push_potential*". Then run the lammps script:

    ./lmp_serial_22Aug2018_push_potential -in commands_lammps_dsDNA_and_polymerase



Copy and paste here both the initial configuration generated before "initial_configuration_lineardsDNA_N500_T50" and the LAMMPS executable "*lmp_serial_22Aug2018*". The c++ executable to change the persistence length of ssDNA (*linear_lp_ssDNA_new_list_cosine_stack_roll_dihedral.out*) should be inside the folder "den_linear_DNA_N500_T50/" before running the LAMMPS script:

    ./lmp_serial_22Aug2018 -in commands_lammps_lineardsDNA_denaturation

Since using the external programm to change the type of interactions produces an abrut change in the dynamics of the DNA model, we suggest to **decrease the LAMMPS timestep** from the usual value of 0.005 to 0.025. Please be aware that this will produce some warnings (FENE bond too long) because the fene bonds are close to the maximum value R_0 set in the lammps script. This is ok and the user should not be worried about it. Finally, we found that sometimes the simulations crash. In this case we suggest to restart from the last file produced during the simulation (called newconfiguration_xxxxxx, where xxxxxx represents the timestep) and change the seed of the integrators. With this the simulation should not crash at the same timestep, but if the problem persists the best approach is to decrease the timestep even more.

Please note that the file "radius_of_gyration.txt" contains empty lines separating the information at different times. To remove these blank spaces it is possible to use for example sed, and type in a terminal:

    sed -i '/^$/d' radius_of_gyration.txt

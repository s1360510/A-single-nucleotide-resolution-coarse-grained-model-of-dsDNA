We model double-stranded DNA (dsDNA) as made of two bead-spring polymers, each representing one single-stranded DNA (ssDNA). In order to couple the two strands into a double-helix, patches are attached to the beads and interaction potentials are assigned to reproduce the geometry of a B-DNA molecule. All these potentials are explained in detail in our soft matter paper (see below). 

Here you will find the c++ codes to create the initial configurations and the LAMMPS scripts to run the simulations of:

    a)Linear and ring DNA molecules without explicit phosphates.
    b)Linear and ring DNA molecules with explicit phosphates.
    c)The protocols for denaturation of linear and ring DNA molecules (only for the case without explicit phosphates).
    d)Transcription by a model polymerase.
    e)Major groove binding proteins.

A brief explanation of the implementation in each of these cases is provided in the **README** files inside the corresponding sub-folders: **initial_configuration** (contains the programm to generate artificial configurations of DNA, either a straight line or a circle) and **equilibration** (contains the LAMMPS script to run the simulation). In some cases, in addition you will find the sub-folder **equilibrated_ configurations** in which you will find files with configurations obtained after running the simmulation for several timesteps (~10^8 ). 

For details in the model see our paper:
Y. A. G. Fosado, D. Michieletto, J. Allan, C. A. Brackley, O. Henrich and D. Marenduzzo "_A single nucleotide resolution model for large-scale simulations of double stranded DNA_" Soft Matter 12 9458-9470 (2016).

For details in the denaturation of linear and ring DNA molecules see:
Y. A. G. Fosado, D. Michieletto, and D. Marenduzzo "_Dynamical Scaling and Phase Coexistence in Topologically Constrained DNA Melting_" Phys. Rev. Lett. 119, 118002 (2017).

All the LAMMPS scripts have been tested using the current version of LAMMPS (**22-Aug-2018**). Pre-built LAMMPS executables can be found in the folder "lammps_executables/" (they will require execute permissions). But in case the user prefers to built his/her own executables, please make sure that the **ASPHERE**, **RIGID**, **MOLECULE** and **USER-MISC** packages are installed. These are standard packages that come with LAMMPS, and can be enabled before compiling, e.g. with the command: make yes-asphere. 

We refer to the executable with these standard packages as *lmp_serial_22Aug2018*. This will be necessary to run most of the scripts.

The simulations for the system with a model polymerase require custom potentials. Further instructions on how to compile the corresponding LAMMPS executables (*lmp_serial_22Aug2018_push_potential* and *lmp_serial_22Aug2018_push_potential_and_new_fix_rigid*) can be found in the README files inside "ring_with_polymerase_fixed_in_space/0_lammps_with_push_potential/" and "ring_with_polymerase_moving_along_the_polymer/0_lammps_with_push_potential_and_rigid_integrators/".





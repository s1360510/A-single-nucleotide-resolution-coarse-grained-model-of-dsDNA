Copy and paste here both the initial configuration generated before "*initial_configuration_lineardsDNA_N1000_T100*" and the LAMMPS executable "*lmp_serial_22Aug2018*". Then run the lammps script: 

    ./lmp_serial_22Aug2018 -in commands_lammps_dsDNA

Please note that since the simulation starts from an artificial configuration that is far from equilibrium (a straight line), at the beggining will appear some warnings (FENE bond too long). The warnings will appear less often once the radius of gyration decreases to a constant value. In addition, we leave some room for the fene bond to change from the average value 0.465 to the maximum value R0=0.6825 set in the lammps script (this is slightly larger than the variations in the real backbone) the **warnings** appear because the bond is getting close to this value, and the user **shouldn't worry** about them (the simulation will not crash).
